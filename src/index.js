const express = require('express');
const app = express();
const PORT = 4000;

app.use(express.static('client'));

app.listen(PORT, () => console.log(`Happystats listening on port ${PORT}`));
