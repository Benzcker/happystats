import generateStatement from "./generateStatement.js";

export default (DOM_statements, statements=[]) => 
{
    DOM_statements.innerHTML = '';
    for (const statement of statements) 
    {
        const DOM_statement = generateStatement(statement);
        DOM_statements.appendChild(DOM_statement);
    }

    const DOM_submit = document.createElement('button');
    DOM_submit.setAttribute('type', 'submit');
    DOM_submit.classList.add('done');

    const DOM_submitIMG = document.createElement('img');
    DOM_submitIMG.setAttribute('src', 'img/done-24px.svg');
    DOM_submitIMG.setAttribute('alt', 'Absenden');
    
    DOM_submit.appendChild(DOM_submitIMG);
    DOM_statements.appendChild(DOM_submit);
}

