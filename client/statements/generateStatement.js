const buttonValues = [1, 2, 3, 4, 5];
export default statement => 
{
    const DOM_statement = document.createElement('div');
    DOM_statement.classList.add('overlay', 'overlay2');

    const DOM_statementText = document.createElement('div');
    DOM_statementText.innerText = statement;
    DOM_statement.appendChild(DOM_statementText);

    const DOM_answer = document.createElement('div');
    DOM_answer.classList.add('answer', 'overlay', 'overlay3');

    const DOM_leftAnswer = document.createElement('span');
    DOM_leftAnswer.innerText = 'Stimmt nicht';
    DOM_leftAnswer.classList.add('answerside');
    DOM_answer.appendChild(DOM_leftAnswer);
    
    for (const buttonValue of buttonValues) 
    {
        const DOM_button = document.createElement('button');
        DOM_button.innerText = buttonValue;
        DOM_answer.appendChild(DOM_button);
    }

    const DOM_rightAnswer = document.createElement('span');
    DOM_rightAnswer.innerText = 'Stimmt';
    DOM_rightAnswer.classList.add('answerside');
    DOM_answer.appendChild(DOM_rightAnswer);

    DOM_statement.appendChild(DOM_answer);
    
    return DOM_statement;
};
