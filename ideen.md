# Was macht uns glücklich?
Umfrage, was uns glücklich macht.
Dazu kann man jeden Abend auf die Webseite gehen und einige Fragen beantworten.
Später bekommt man einen Bericht über seine Daten per Mail zugesandt. Dieser wird mit R erstellt.
Er zeigt Abhängigkeiten, Durchschnitte, Trends usw.

## Fragen
Alle Fragen sind Werte, die von 1(trifft nicht zu) bis 5(trifft voll zu) gehen.

1. Ich hatte heute einen glücklichen Tag
2. Ich habe mir heute viel Zeit für mich genommen
3. Ich habe heute Freizeit mit Freunden verbracht
4. Ich habe heute viel Zeit vor Bildschirmen verbracht
5. Ich habe heute viel gearbeitet
6. Ich habe heute anderen geholfen
7. Ich habe heute sehr gesund gegessen
8. Ich habe heute viel genascht

## Nutzer
Man muss sich anmelden, damit die Daten auch auf Nutzer bezogen werden können

## Monatsmeldung
Jeden Monat bekommen alle Nutzer eine Mail mit ihren über R ausgewerteten Daten.
Eventuell gibt es auch einen Überblick über die Daten aller Nutzer.
